CONTRIBUTING
============

Submit patches, bug reports, and questions for any of the maintainer tools and
documentation to the dim-tools@lists.freedesktop.org mailing list.

Please make sure your patches pass the build and self tests by running::

  $ make check

Push the patches once you have an ack from maintainers (Jani/Daniel).
